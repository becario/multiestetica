import {ListModel} from '../pages/home/list.model';

export interface IAppState {
    lists: ListModel[],
    searchTerm: string
}

export const INITIAL_STATE: IAppState = {
    lists: [],
    searchTerm: ''
};

export function rootReducer(state: IAppState, action): IAppState {

    switch (action.type) {
        case 'UPDATE_LISTS':
            state.lists = action.body;
            return state;
        case 'UPDATE_SEARCH_TERM':
            state.searchTerm = action.body;
    }

    return state;

}