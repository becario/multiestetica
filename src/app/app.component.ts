import {Component} from '@angular/core';
import {Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {

    public rootPage: any = HomePage;
    public menuOptions: string[];

    constructor(
        platform: Platform,
        statusBar: StatusBar,
        splashScreen: SplashScreen
    ) {
        this.menuOptions = [
            'Experiencias',
            'Tratamientos',
            'Doctores',
            'Ofertas',
            'Pregunta al doctor',
            'Foro'
        ];

        platform.ready().then(() => {
            statusBar.styleDefault();
            statusBar.hide();
            splashScreen.hide();
        });
    }
}
