import {NgModule} from '@angular/core';

import {
    NgRedux,
    NgReduxModule,
    DevToolsExtension
} from '@angular-redux/store';
import {
    IAppState,
    rootReducer,
    INITIAL_STATE
} from './store';

@NgModule({
    imports: [NgReduxModule]
})

export class AppReduxModule {

    constructor(
        devTools: DevToolsExtension, // <--- nice chrome plugin to inspect the Redux store.
        public ngRedux: NgRedux<IAppState>
    ) {
        ngRedux.configureStore(
            rootReducer,
            INITIAL_STATE,
            [],
            devTools.isEnabled() ? [ devTools.enhancer() ] : []
        );
    }

}