import {
    NgModule,
    ErrorHandler
} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {
    IonicApp,
    IonicModule,
    IonicErrorHandler
} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {AppReduxModule} from './app.redux';
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListComponent} from '../components/list/list.component';
import {TagsComponent} from '../components/tags/tags.component';
import {HomeService} from '../pages/home/home.service';

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        TagsComponent,
        ListComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        AppReduxModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        },
        HomeService
    ]
})
export class AppModule {
}
