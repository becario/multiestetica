import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {NgRedux} from '@angular-redux/store';

import {IAppState} from '../../app/store';
import {ListModel} from './list.model';
import {HomeService} from './home.service';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    public lists: ListModel[];
    public searchTerm: string;

    constructor(
        public navCtrl: NavController,
        public ngRedux: NgRedux<IAppState>,
        private homeService: HomeService
    ) {
        this.retrieveData();
    }

    public updateSearch(term: string): void {
        this.ngRedux.dispatch({
            type: 'UPDATE_SEARCH_TERM',
            body: term
        } as any);
    }

    private retrieveData(): void {
        this.lists = this.homeService.getLists();

        this.ngRedux.dispatch({
            type: 'UPDATE_LISTS',
            body: this.lists
        } as any);
    }

}
