import {ListElementModel} from '../../components/list/list-element.model';

export class ListModel {

    public title: string;
    public image: string;
    public background: string;
    public elements: ListElementModel[] = [];

}