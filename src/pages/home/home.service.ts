import {Injectable} from '@angular/core';

import {ListModel} from './list.model';
import {ListElementModel} from '../../components/list/list-element.model';

@Injectable()
export class HomeService {

    private lists: ListModel[];

    constructor(){}

    public getLists(): ListModel[] {
        this.lists = [
            new ListModel(),
            new ListModel()
        ];

        this.populateList0();
        this.populateList1();

        return this.lists;
    }



    private populateList0(): void {
        const listData = [
            {
                title: 'Micropigmentación',
                subitems: 112
            },
            {
                title: 'Rinoplastia',
                subitems: 309
            },
            {
                title: 'Otoplastia',
                subitems: 77
            },
            {
                title: 'Aumento labios',
                subitems: 154
            },
            {
                title: 'Blefaroplastia',
                subitems: 77
            }
        ];

        this.lists[0].title = 'Cara y cuello';
        this.lists[0].image = 'assets/imgs/cara-y-cuello-13-rounded.png';
        this.lists[0].background = 'assets/imgs/cara-y-cuello-13-desktop.jpg';
        listData.forEach(
            (elem) => {
                this.lists[0].elements.push(
                    new ListElementModel(
                        elem.title,
                        elem.subitems,
                        false
                    )
                );
            }
        );
    }

    private populateList1(): void {
        const listData = [
            {
                title: 'Abdominoplastia',
                subitems: 449
            },
            {
                title: 'Mesoterapia',
                subitems: 84
            },
            {
                title: 'Liposucción',
                subitems: 320
            },
            {
                title: 'Cavitación',
                subitems: 31
            },
            {
                title: 'Lipoescultura',
                subitems: 186
            },
            {
                title: 'Tratamiento varices',
                subitems: 105
            },
            {
                title: 'Drenaje linfático',
                subitems: 22
            },
            {
                title: 'Ginecomastia',
                subitems: 28
            },
            {
                title: 'Aumento de glúteos',
                subitems: 83
            },
            {
                title: 'Cirujía del pie',
                subitems: 5
            },
            {
                title: 'Braquioplástia',
                subitems: 8
            },
            {
                title: 'Aumento gemelos',
                subitems: 11
            },
            {
                title: 'Aumento pectorales',
                subitems: 3
            },
            {
                title: 'Cirugía vascular',
                subitems: 8
            },
            {
                title: 'Lipoláser',
                subitems: 85
            },
            {
                title: 'Crioliposis',
                subitems: 19
            },
            {
                title: 'Hidrolipoclásia',
                subitems: 22
            },
            {
                title: 'Coolsculpting',
                subitems: 5
            },
            {
                title: 'Intralipoterapia',
                subitems: 9
            },
            {
                title: 'Eliminación de estrias',
                subitems: 116
            },
            {
                title: 'Lipoliftinc',
                subitems: 17
            },
            {
                title: 'Láser vascular',
                subitems: 34
            },
            {
                title: 'Crioterápia',
                subitems: 5
            },
            {
                title: 'Celulitis',
                subitems: 107
            },
            {
                title: 'Tratamiento post-liposucción',
                subitems: 4
            },
            {
                title: 'Carboxiterapia',
                subitems: 31
            },
            {
                title: 'Tratamiento antigrasa',
                subitems: 24
            },
            {
                title: 'Electroestimulación',
                subitems: 6
            },
            {
                title: 'Presoterapia',
                subitems: 20
            },
            {
                title: 'Termoterápia',
                subitems: 4
            },
            {
                title: 'Endermología',
                subitems: 6
            },
            {
                title: 'Tratamientos reafirmantes',
                subitems: 60
            },
            {
                title: 'Marcación abdominal',
                subitems: 7
            },
            {
                title: 'Hondas de choque',
                subitems: 4
            },
            {
                title: 'Radiofrecuencia',
                subitems: 9
            },
            {
                title: 'HIFU',
                subitems: 6
            },
            {
                title: 'Lipolaser',
                subitems: 2
            }
        ];

        this.lists[1].title = 'Cuerpo';
        this.lists[1].image = 'assets/imgs/cuerpo-13-rounded.png';
        this.lists[1].background = 'assets/imgs/cuerpo-13-desktop.jpg';
        listData.forEach(
            (elem) => {
                this.lists[1].elements.push(
                    new ListElementModel(
                        elem.title,
                        elem.subitems,
                        false
                    )
                );
            }
        );
    }
}