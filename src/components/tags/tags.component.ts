import {Component} from '@angular/core';
import {NgRedux} from '@angular-redux/store';

import {IAppState} from '../../app/store';
import {ListElementModel} from '../list/list-element.model';

@Component({
    selector: 'me-tags',
    templateUrl: 'tags.component.html'
})

export class TagsComponent {

    public tags: string[];

    constructor(
        public ngRedux: NgRedux<IAppState>
    ) {}

    public removeTag(element: ListElementModel): void {
        element.state = false;
    }


}