export class ListElementModel {

    public title: string;
    public subItemCount: number;
    public state: boolean;


    constructor(
        title: string,
        subItemCount: number,
        state: boolean
    ) {
        this.title = title;
        this.subItemCount = subItemCount;
        this.state = state;
    }
}