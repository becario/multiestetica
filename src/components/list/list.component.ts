import {
    Component,
    Input
} from '@angular/core';
import {NgRedux} from '@angular-redux/store';

import {IAppState} from '../../app/store';
import {ListModel} from '../../pages/home/list.model';
import {ListElementModel} from './list-element.model';

@Component({
    selector: 'me-list',
    templateUrl: 'list.component.html'
})
export class ListComponent {

    public foldStatus: boolean;
    @Input() private list: ListModel;
    @Input() private listIndex: number;

    constructor(public ngRedux: NgRedux<IAppState>) {
        this.foldStatus = true;
    }

    public updateReduxStore(index: number, newValue: boolean): void {
        const currentLists = this.ngRedux.getState().lists;

        this.list.elements[index].state = newValue;
        currentLists[this.listIndex].elements[index].state = this.list.elements[index].state;
        this.ngRedux.dispatch({
            type: 'UPDATE_LISTS',
            body: currentLists
        } as any);
    }

    public searchTermMatch(title: string): boolean {
        const searchTerm = this.ngRedux.getState().searchTerm.toLowerCase();

        if (searchTerm && searchTerm.length > 0) {
            if ( title.toLowerCase().indexOf(searchTerm) > -1){
                return true;

            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public getTotalListVisibleElements(list: ListModel): number {
        let count = 0;

        list.elements.forEach(
            (element: ListElementModel) => {
                if (this.searchTermMatch(element.title)) {
                    count ++;
                }
            }
        );

        return count;
    }

}